# Thinkpad Trackpoint keyboard x QMK x Teensy 4.0
The goal of this project is to have a QMK enabled Thinkpad Trackpoint keyboard.

## Motivation
I changed jobs and I now have to use a Windows computer with an AZERTY keyboard.
I use this [international UK QWERTY](https://en.wikipedia.org/wiki/QWERTY#/media/File:UKIntlUbuntu.png) layout which is not available in windows.
I also remap capslock to ctrl.

It turns out doing this on windows is a major PITA:

- I have to setup several keyboards and use `win+space` to switch between them.
- When sharing my screen and taking over control of a computer via teams (corporate tooling) the layout is completely messed up.
It's neither AZERTY nor QWERTY.
- Sometimes the caps-lock remap stops working and I'm stuck with a locked capslock.

I tried using:

- [MKLC](https://support.microsoft.com/en-us/topic/906c31e4-d5ea-7988-cb39-7b688880d7cb) but it's really hard to iterate over a keyboard layout.
I ended up accumulating several custom keyboard layouts and I don't really know which one is the correct one.
- [PowerToys](https://docs.microsoft.com/en-us/windows/powertoys/) to remap caps-lock to ctrl.
But it's not really reliable. Sometimes caps-lock

I didn't try:
- [kmonad](https://github.com/kmonad/kmonad)

## Inspirations
- This [instructable](https://www.instructables.com/How-to-Make-a-USB-Laptop-Keyboard-Controller/).
- [Repair of the Bluetooth version](http://bdm.cc/2016/06/lenovo-bluetooth-keyboard-repairs/) with some hardware details.
