EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Keyboard to teensy 4.0"
Date ""
Rev "0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x28_Female J2
U 1 1 60C927A5
P 8350 2900
F 0 "J2" V 8515 2830 50  0000 C CNN
F 1 "Conn_01x28_Female" V 8424 2830 50  0000 C CNN
F 2 "JLCPCB:FPC-SMD_AFA07-S28FCC-00_JLCPCB_C262357" H 8350 2900 50  0001 C CNN
F 3 "~" H 8350 2900 50  0001 C CNN
F 4 "C262357" V 8350 2900 50  0001 C CNN "BOM"
	1    8350 2900
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 60C9C0C4
P 2650 2850
F 0 "J1" H 2678 2826 50  0000 L CNN
F 1 "Conn_01x04_Female" H 2678 2735 50  0000 L CNN
F 2 "JLCPCB:FPC-SMD_4P_JLCPCB_C555410" H 2650 2850 50  0001 C CNN
F 3 "~" H 2650 2850 50  0001 C CNN
F 4 "C555410" H 2650 2850 50  0001 C CNN "BOM"
	1    2650 2850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H9
U 1 1 60CA3132
P 3000 4550
F 0 "H9" H 2900 4507 50  0000 R CNN
F 1 "Connector_Pin" H 2900 4598 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 3000 4550 50  0001 C CNN
F 3 "~" H 3000 4550 50  0001 C CNN
	1    3000 4550
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H10
U 1 1 60CBDE18
P 3000 4850
F 0 "H10" H 2900 4807 50  0000 R CNN
F 1 "Connector_Pin" H 2900 4898 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 3000 4850 50  0001 C CNN
F 3 "~" H 3000 4850 50  0001 C CNN
	1    3000 4850
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H11
U 1 1 60CBE312
P 3000 5150
F 0 "H11" H 2900 5107 50  0000 R CNN
F 1 "Connector_Pin" H 2900 5198 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 3000 5150 50  0001 C CNN
F 3 "~" H 3000 5150 50  0001 C CNN
	1    3000 5150
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H12
U 1 1 60CBFA42
P 3000 5450
F 0 "H12" H 2900 5407 50  0000 R CNN
F 1 "Connector_Pin" H 2900 5498 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 3000 5450 50  0001 C CNN
F 3 "~" H 3000 5450 50  0001 C CNN
	1    3000 5450
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H13
U 1 1 60CC1CF9
P 3000 5800
F 0 "H13" H 2900 5757 50  0000 R CNN
F 1 "Connector_Pin" H 2900 5848 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 3000 5800 50  0001 C CNN
F 3 "~" H 3000 5800 50  0001 C CNN
	1    3000 5800
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H14
U 1 1 60CC1CFF
P 3000 6100
F 0 "H14" H 2900 6057 50  0000 R CNN
F 1 "Connector_Pin" H 2900 6148 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 3000 6100 50  0001 C CNN
F 3 "~" H 3000 6100 50  0001 C CNN
	1    3000 6100
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H15
U 1 1 60CC1D05
P 3000 6400
F 0 "H15" H 2900 6357 50  0000 R CNN
F 1 "Connector_Pin" H 2900 6448 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 3000 6400 50  0001 C CNN
F 3 "~" H 3000 6400 50  0001 C CNN
	1    3000 6400
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H16
U 1 1 60CC1D0B
P 3000 6700
F 0 "H16" H 2900 6657 50  0000 R CNN
F 1 "Connector_Pin" H 2900 6748 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 3000 6700 50  0001 C CNN
F 3 "~" H 3000 6700 50  0001 C CNN
	1    3000 6700
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H17
U 1 1 60CC4C63
P 4400 4550
F 0 "H17" H 4300 4507 50  0000 R CNN
F 1 "Connector_Pin" H 4300 4598 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 4400 4550 50  0001 C CNN
F 3 "~" H 4400 4550 50  0001 C CNN
	1    4400 4550
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H18
U 1 1 60CC4C69
P 4400 4850
F 0 "H18" H 4300 4807 50  0000 R CNN
F 1 "Connector_Pin" H 4300 4898 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 4400 4850 50  0001 C CNN
F 3 "~" H 4400 4850 50  0001 C CNN
	1    4400 4850
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H19
U 1 1 60CC4C6F
P 4400 5150
F 0 "H19" H 4300 5107 50  0000 R CNN
F 1 "Connector_Pin" H 4300 5198 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 4400 5150 50  0001 C CNN
F 3 "~" H 4400 5150 50  0001 C CNN
	1    4400 5150
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H20
U 1 1 60CC4C75
P 4400 5450
F 0 "H20" H 4300 5407 50  0000 R CNN
F 1 "Connector_Pin" H 4300 5498 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 4400 5450 50  0001 C CNN
F 3 "~" H 4400 5450 50  0001 C CNN
	1    4400 5450
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H21
U 1 1 60CC52FF
P 4400 5800
F 0 "H21" H 4300 5757 50  0000 R CNN
F 1 "Connector_Pin" H 4300 5848 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 4400 5800 50  0001 C CNN
F 3 "~" H 4400 5800 50  0001 C CNN
	1    4400 5800
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H22
U 1 1 60CC5305
P 4400 6100
F 0 "H22" H 4300 6057 50  0000 R CNN
F 1 "Connector_Pin" H 4300 6148 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 4400 6100 50  0001 C CNN
F 3 "~" H 4400 6100 50  0001 C CNN
	1    4400 6100
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H23
U 1 1 60CC530B
P 4400 6400
F 0 "H23" H 4300 6357 50  0000 R CNN
F 1 "Connector_Pin" H 4300 6448 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 4400 6400 50  0001 C CNN
F 3 "~" H 4400 6400 50  0001 C CNN
	1    4400 6400
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H24
U 1 1 60CC5311
P 4400 6700
F 0 "H24" H 4300 6657 50  0000 R CNN
F 1 "Connector_Pin" H 4300 6748 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 4400 6700 50  0001 C CNN
F 3 "~" H 4400 6700 50  0001 C CNN
	1    4400 6700
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H25
U 1 1 60CC5E7D
P 5850 4550
F 0 "H25" H 5750 4507 50  0000 R CNN
F 1 "Connector_Pin" H 5750 4598 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 5850 4550 50  0001 C CNN
F 3 "~" H 5850 4550 50  0001 C CNN
	1    5850 4550
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H26
U 1 1 60CC5E83
P 5850 4850
F 0 "H26" H 5750 4807 50  0000 R CNN
F 1 "Connector_Pin" H 5750 4898 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 5850 4850 50  0001 C CNN
F 3 "~" H 5850 4850 50  0001 C CNN
	1    5850 4850
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H27
U 1 1 60CC5E89
P 5850 5150
F 0 "H27" H 5750 5107 50  0000 R CNN
F 1 "Connector_Pin" H 5750 5198 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 5850 5150 50  0001 C CNN
F 3 "~" H 5850 5150 50  0001 C CNN
	1    5850 5150
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 60CC73FD
P 1750 4500
F 0 "H1" H 1650 4457 50  0000 R CNN
F 1 "Connector_Pin" H 1650 4548 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 1750 4500 50  0001 C CNN
F 3 "~" H 1750 4500 50  0001 C CNN
	1    1750 4500
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 60CC7403
P 1750 4800
F 0 "H2" H 1650 4757 50  0000 R CNN
F 1 "Connector_Pin" H 1650 4848 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 1750 4800 50  0001 C CNN
F 3 "~" H 1750 4800 50  0001 C CNN
	1    1750 4800
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 60CC7409
P 1750 5100
F 0 "H3" H 1650 5057 50  0000 R CNN
F 1 "Connector_Pin" H 1650 5148 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 1750 5100 50  0001 C CNN
F 3 "~" H 1750 5100 50  0001 C CNN
	1    1750 5100
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 60CC740F
P 1750 5400
F 0 "H4" H 1650 5357 50  0000 R CNN
F 1 "Connector_Pin" H 1650 5448 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 1750 5400 50  0001 C CNN
F 3 "~" H 1750 5400 50  0001 C CNN
	1    1750 5400
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 60CC813D
P 1750 5800
F 0 "H5" H 1650 5757 50  0000 R CNN
F 1 "Connector_Pin" H 1650 5848 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 1750 5800 50  0001 C CNN
F 3 "~" H 1750 5800 50  0001 C CNN
	1    1750 5800
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 60CC8143
P 1750 6100
F 0 "H6" H 1650 6057 50  0000 R CNN
F 1 "Connector_Pin" H 1650 6148 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 1750 6100 50  0001 C CNN
F 3 "~" H 1750 6100 50  0001 C CNN
	1    1750 6100
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H7
U 1 1 60CC8149
P 1750 6400
F 0 "H7" H 1650 6357 50  0000 R CNN
F 1 "Connector_Pin" H 1650 6448 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 1750 6400 50  0001 C CNN
F 3 "~" H 1750 6400 50  0001 C CNN
	1    1750 6400
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H8
U 1 1 60CC814F
P 1750 6700
F 0 "H8" H 1650 6657 50  0000 R CNN
F 1 "Connector_Pin" H 1650 6748 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 1750 6700 50  0001 C CNN
F 3 "~" H 1750 6700 50  0001 C CNN
	1    1750 6700
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad TP1
U 1 1 60CCA57B
P 700 4450
F 0 "TP1" H 600 4407 50  0000 R CNN
F 1 "Connector_Pin" H 600 4498 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 700 4450 50  0001 C CNN
F 3 "~" H 700 4450 50  0001 C CNN
	1    700  4450
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad TP2
U 1 1 60CCA581
P 700 4750
F 0 "TP2" H 600 4707 50  0000 R CNN
F 1 "Connector_Pin" H 600 4798 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 700 4750 50  0001 C CNN
F 3 "~" H 700 4750 50  0001 C CNN
	1    700  4750
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad TP3
U 1 1 60CCA587
P 700 5050
F 0 "TP3" H 600 5007 50  0000 R CNN
F 1 "Connector_Pin" H 600 5098 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 700 5050 50  0001 C CNN
F 3 "~" H 700 5050 50  0001 C CNN
	1    700  5050
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad TP4
U 1 1 60CCA58D
P 700 5350
F 0 "TP4" H 600 5307 50  0000 R CNN
F 1 "Connector_Pin" H 600 5398 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 700 5350 50  0001 C CNN
F 3 "~" H 700 5350 50  0001 C CNN
	1    700  5350
	-1   0    0    1   
$EndComp
Wire Wire Line
	700  4350 700  2750
Wire Wire Line
	700  2750 2450 2750
Wire Wire Line
	700  4650 1550 4650
Wire Wire Line
	1550 4650 1550 2850
Wire Wire Line
	1550 2850 2450 2850
Wire Wire Line
	700  5250 1650 5250
Wire Wire Line
	1650 5250 1650 3050
Wire Wire Line
	1650 3050 2450 3050
Wire Wire Line
	700  4950 1600 4950
Wire Wire Line
	1600 4950 1600 2950
Wire Wire Line
	1600 2950 2450 2950
Wire Wire Line
	1750 4400 1750 3100
Wire Wire Line
	1750 3100 7050 3100
Wire Wire Line
	1750 4700 2550 4700
Wire Wire Line
	2550 4700 2550 3150
Wire Wire Line
	2550 3150 7150 3150
Wire Wire Line
	7150 3150 7150 3100
Wire Wire Line
	1750 5000 2600 5000
Wire Wire Line
	2600 5000 2600 3200
Wire Wire Line
	2600 3200 7250 3200
Wire Wire Line
	7250 3200 7250 3100
Wire Wire Line
	1750 5300 2650 5300
Wire Wire Line
	2650 5300 2650 3250
Wire Wire Line
	2650 3250 7350 3250
Wire Wire Line
	7350 3250 7350 3100
Wire Wire Line
	1750 5700 2700 5700
Wire Wire Line
	2700 5700 2700 3300
Wire Wire Line
	2700 3300 7450 3300
Wire Wire Line
	7450 3300 7450 3100
Wire Wire Line
	1750 6000 2750 6000
Wire Wire Line
	2750 6000 2750 3350
Wire Wire Line
	2750 3350 7550 3350
Wire Wire Line
	7550 3350 7550 3100
Wire Wire Line
	1750 6300 2800 6300
Wire Wire Line
	2800 6300 2800 3400
Wire Wire Line
	2800 3400 7650 3400
Wire Wire Line
	7650 3400 7650 3100
Wire Wire Line
	1750 6600 2850 6600
Wire Wire Line
	2850 6600 2850 3450
Wire Wire Line
	2850 3450 7750 3450
Wire Wire Line
	7750 3450 7750 3100
Wire Wire Line
	3000 4450 3000 3500
Wire Wire Line
	7850 3500 7850 3100
Wire Wire Line
	3000 3500 7850 3500
Wire Wire Line
	3000 4750 3800 4750
Wire Wire Line
	3800 4750 3800 3550
Wire Wire Line
	3800 3550 7950 3550
Wire Wire Line
	7950 3550 7950 3100
Wire Wire Line
	3000 5050 3850 5050
Wire Wire Line
	3850 5050 3850 3600
Wire Wire Line
	3850 3600 8050 3600
Wire Wire Line
	8050 3600 8050 3100
Wire Wire Line
	3000 5350 3900 5350
Wire Wire Line
	3900 5350 3900 3650
Wire Wire Line
	3900 3650 8150 3650
Wire Wire Line
	8150 3650 8150 3100
Wire Wire Line
	3000 5700 3950 5700
Wire Wire Line
	3950 5700 3950 3700
Wire Wire Line
	3950 3700 8250 3700
Wire Wire Line
	8250 3700 8250 3100
Wire Wire Line
	3000 6000 4000 6000
Wire Wire Line
	4000 6000 4000 3750
Wire Wire Line
	4000 3750 8350 3750
Wire Wire Line
	8350 3750 8350 3100
Wire Wire Line
	3000 6300 4050 6300
Wire Wire Line
	4050 6300 4050 3800
Wire Wire Line
	4050 3800 8450 3800
Wire Wire Line
	8450 3800 8450 3100
Wire Wire Line
	3000 6600 4100 6600
Wire Wire Line
	4100 6600 4100 3850
Wire Wire Line
	4100 3850 8550 3850
Wire Wire Line
	8550 3850 8550 3100
Wire Wire Line
	4400 4450 4400 3900
Wire Wire Line
	4400 3900 8650 3900
Wire Wire Line
	8650 3900 8650 3100
Wire Wire Line
	4400 4750 5200 4750
Wire Wire Line
	5200 4750 5200 3950
Wire Wire Line
	5200 3950 8750 3950
Wire Wire Line
	8750 3950 8750 3100
Wire Wire Line
	4400 5050 5250 5050
Wire Wire Line
	5250 5050 5250 4000
Wire Wire Line
	5250 4000 8850 4000
Wire Wire Line
	8850 4000 8850 3100
Wire Wire Line
	4400 5350 5300 5350
Wire Wire Line
	5300 5350 5300 4050
Wire Wire Line
	5300 4050 8950 4050
Wire Wire Line
	8950 4050 8950 3100
Wire Wire Line
	4400 5700 5350 5700
Wire Wire Line
	5350 5700 5350 4100
Wire Wire Line
	5350 4100 9050 4100
Wire Wire Line
	9050 4100 9050 3100
Wire Wire Line
	4400 6000 5400 6000
Wire Wire Line
	5400 6000 5400 4150
Wire Wire Line
	5400 4150 9150 4150
Wire Wire Line
	9150 4150 9150 3100
Wire Wire Line
	4400 6300 5450 6300
Wire Wire Line
	5450 6300 5450 4200
Wire Wire Line
	5450 4200 9250 4200
Wire Wire Line
	9250 4200 9250 3100
Wire Wire Line
	4400 6600 5500 6600
Wire Wire Line
	5500 6600 5500 4250
Wire Wire Line
	5500 4250 9350 4250
Wire Wire Line
	9350 4250 9350 3100
Wire Wire Line
	5850 4450 9450 4450
Wire Wire Line
	9450 4450 9450 3100
Wire Wire Line
	5850 4750 9550 4750
Wire Wire Line
	9550 4750 9550 3100
Wire Wire Line
	5850 5050 9650 5050
Wire Wire Line
	9650 5050 9650 3100
Wire Wire Line
	5850 5350 9750 5350
Wire Wire Line
	9750 5350 9750 3100
$Comp
L Mechanical:MountingHole_Pad H28
U 1 1 60CC5E8F
P 5850 5450
F 0 "H28" H 5750 5407 50  0000 R CNN
F 1 "Connector_Pin" H 5750 5498 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 5850 5450 50  0001 C CNN
F 3 "~" H 5850 5450 50  0001 C CNN
	1    5850 5450
	-1   0    0    1   
$EndComp
$EndSCHEMATC
